const express = require('express')
const bodyparser = require('body-parser')
const path = require('path')
const helmet = require('helmet')

const apiRoute = require('./src/route/apiRoute')

const PORT = process.env.PORT || 3000

let app = express();

app.use(helmet())
app.use(bodyparser.json())

app.use('/api', apiRoute)

app.get('/', (req, res) => {
  res.send('Hello World')
})

app.listen(3000, _ => {
  console.log(`listen port ${PORT}`)
})