const fetch = require('node-fetch')
const settings = require('../../../settings.json')

const {
    GraphQLSchema, 
    GraphQLObjectType,
    GraphQLInt,
    GraphQLString
} = require('graphql')

const PokemonType = new GraphQLObjectType({
    name: 'pokemon',
    description: '...',
    fields: () => ({
        id: {
            type: GraphQLInt,
            resolve: object => object.id
        },
        name: {
            type: GraphQLString,
            resolve: object => object.name
        },
    })
})

let query = new GraphQLObjectType({

    name: "PokemonQuery",
    description: "...",
    fields: () =>  ({
        pokemon: { 
            type: PokemonType,
            args: {
                id: {type: GraphQLInt}
            },
            resolve: (root, args) => fetch(`${settings.apiUrl}${args.id}`)
            .then(blob => blob.json())
            .then(data => data)
        }
    })
})

module.exports = new GraphQLSchema({
    query
});