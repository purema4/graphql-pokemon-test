const express = require('express')
const router = express.Router()

const fetch = require('node-fetch')

const graphqlHTTP = require('express-graphql')
const pokemonSchema = require('../graphql/schema/pokemonSchema')

router.use('/pokemon', graphqlHTTP({
    schema: pokemonSchema,
    graphiql: true,
  }))

router.get('/:id', (req, res) => {
  //console.log(req.params)
  fetch(
    `http://localhost:3000/api/pokemon`, {
      method: 'post',
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json, test/plain'
      },
      body: JSON.stringify({
        query: `{pokemon(id: ${req.params.id}){name}}`
      })
  }).then(blob => blob.json())
  .then(data => res.send(data))
})

module.exports = router